package doorthree.testtask.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import doorthree.testtask.R;

public class PrefUtil {
    public static final String APPDATA_PARAM = "APPDATA";

    public static void saveDataToPreferences(Context context, String data) {
        SharedPreferences.Editor editor = context.getSharedPreferences(
                context.getString(R.string.app_name), Context.MODE_PRIVATE).edit();
        editor.putString(APPDATA_PARAM, data);
        editor.apply();
    }

    public static String getDataFromPreferences(Context context) {
        return context.getSharedPreferences(context.getString(R.string.app_name),
                Context.MODE_PRIVATE).getString(APPDATA_PARAM, "");
    }
}