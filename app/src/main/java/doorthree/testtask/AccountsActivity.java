package doorthree.testtask;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import doorthree.testtask.bus.AddContactEvent;
import doorthree.testtask.bus.MakeToastEvent;
import doorthree.testtask.bus.PopBackStackEvent;
import doorthree.testtask.bus.SaveDataEvent;
import doorthree.testtask.fragments.ContactFragment;
import doorthree.testtask.fragments.ContactsFragment;
import doorthree.testtask.util.PrefUtil;

public class AccountsActivity extends ActionBarActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_accounts);

        // only portrait mode allowed so we don't care about type of passing params into fragment
        ContactsFragment fragment = new ContactsFragment();
        if (savedInstanceState != null) {
            if (savedInstanceState.containsKey(PrefUtil.APPDATA_PARAM)) {
                String data = savedInstanceState.getString(PrefUtil.APPDATA_PARAM);
                fragment.initWithDataString(data);
            }
        }

        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.container, fragment, ContactsFragment.FRAGMENT_TAG)
                .commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.accounts, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
                getSupportFragmentManager().popBackStackImmediate();
                invalidateOptionsMenu();
            }
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        App.BUS.register(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        App.BUS.unregister(this);
    }

    @Override
    protected void onStop() {
        App.BUS.post(new SaveDataEvent(null));
        super.onStop();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        Fragment fragment = getSupportFragmentManager().findFragmentByTag(ContactsFragment.FRAGMENT_TAG);
        if (fragment != null) {
            outState.putString(PrefUtil.APPDATA_PARAM, ((ContactsFragment) fragment).getDataString());
        }

        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() == 0) { // exit
            App.BUS.post(new SaveDataEvent(null));
        }

        super.onBackPressed();
    }

    @com.squareup.otto.Subscribe
    public void onPopBackStackEvent(PopBackStackEvent evt) {
        getSupportFragmentManager().popBackStackImmediate();
        invalidateOptionsMenu();
    }

    @com.squareup.otto.Subscribe
    public void makeToast(final MakeToastEvent evt) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(AccountsActivity.this, evt.text, evt.length).show();
            }
        });
    }

    @com.squareup.otto.Subscribe
    public void onAddContactEvent(AddContactEvent evt) {
        getSupportFragmentManager()
            .beginTransaction()
                // only portrait mode allowed so we don't care about type of passing params into fragment
                // and just passing they via constructor
            .replace(R.id.container, new ContactFragment(evt.contact, evt.addMode), ContactFragment.FRAGMENT_TAG)
            .addToBackStack(null)
            .commit();
    }
}