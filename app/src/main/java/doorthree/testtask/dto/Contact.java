package doorthree.testtask.dto;

public class Contact {
    public int id = -1;
    public String imageUrl;
    public String userName;
    public String userEmail;
    public String location;
}