package doorthree.testtask.dto;

import java.util.List;

public class ContactList {
    public List<Contact> contactList;

    public ContactList(List<Contact> contactList) {
        this.contactList = contactList;
    }
}
