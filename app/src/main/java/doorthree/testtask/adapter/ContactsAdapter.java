package doorthree.testtask.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.List;

import doorthree.testtask.R;
import doorthree.testtask.dto.Contact;

public class ContactsAdapter extends ArrayAdapter<Contact> {
    public static final File EMPTY_FILE = new File("");
    private List<Contact> contacts;
    private LayoutInflater inflater;
    private boolean dragEnabled;

    public ContactsAdapter(Context context, List<Contact> contacts, boolean dragEnabled) {
        super(context, R.layout.contact_list_item, contacts);

        this.contacts = contacts;
        this.inflater = LayoutInflater.from(context);
        this.dragEnabled = dragEnabled;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ContactViewHolder viewHolder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.contact_list_item, null);
            viewHolder = new ContactViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ContactViewHolder) convertView.getTag();
        }

        viewHolder.setContact(getItem(position));
        getItem(position).id = position;
        return convertView;
    }

    public void setDragEnabled(boolean dragEnabled) {
        this.dragEnabled = dragEnabled;
        notifyDataSetChanged();
    }

    private class ContactViewHolder {
        ImageView dragView;
        ImageView userAvatar;
        TextView userName;
        TextView userEmail;

        public ContactViewHolder(View view) {
            dragView = (ImageView) view.findViewById(R.id.dragView);
            userAvatar = (ImageView) view.findViewById(R.id.userAvatar);
            userName = (TextView) view.findViewById(R.id.userName);
            userEmail = (TextView) view.findViewById(R.id.userEmail);
        }

        public void setContact(Contact contact) {
            dragView.setVisibility(!dragEnabled ? View.GONE : View.VISIBLE);
            userName.setText(contact.userName);
            userEmail.setText(contact.userEmail);

            setAvatar(contact);
        }

        private void setAvatar(Contact contact) {
            Picasso.with(getContext()).cancelRequest(userAvatar);
            Picasso.with(getContext())
                .load(contact.imageUrl == null ? EMPTY_FILE : new File(contact.imageUrl))
                .resizeDimen(R.dimen.useravatar_size, R.dimen.useravatar_size)
                .placeholder(R.drawable.ic_launcher)
                .noFade()
                .centerCrop()
                .into(userAvatar);
        }
    }

    public List<Contact> getContacts() {
        return contacts;
    }
}