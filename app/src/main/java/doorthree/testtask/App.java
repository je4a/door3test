package doorthree.testtask;

import android.app.Application;

import com.squareup.otto.Bus;
import com.squareup.otto.ThreadEnforcer;

public class App extends Application {
    public static final Bus BUS = new Bus(ThreadEnforcer.ANY);
}