package doorthree.testtask.bus;

import android.os.Bundle;

public class SaveDataEvent {
    public Bundle toBundle;

    public SaveDataEvent(Bundle toBundle) {
        this.toBundle = toBundle;
    }
}
