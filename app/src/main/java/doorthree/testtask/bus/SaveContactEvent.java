package doorthree.testtask.bus;

import doorthree.testtask.dto.Contact;

public class SaveContactEvent {
    public Contact contact;
    public boolean addMode;

    public SaveContactEvent(Contact contact, boolean addMode) {
        this.contact = contact;
        this.addMode = addMode;
    }
}
