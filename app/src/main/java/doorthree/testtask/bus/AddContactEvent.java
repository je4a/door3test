package doorthree.testtask.bus;

import doorthree.testtask.dto.Contact;

public class AddContactEvent {
    public Contact contact;
    public boolean addMode;

    public AddContactEvent(Contact contact, boolean addMode) {
        this.contact = contact;
        this.addMode = addMode;
    }
}
