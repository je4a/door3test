package doorthree.testtask.bus;

public class MakeToastEvent {
    public String text;
    public int length;

    public MakeToastEvent(String text, int length) {
        this.length = length;
        this.text = text;
    }
}
