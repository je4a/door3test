package doorthree.testtask.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import doorthree.testtask.AccountsActivity;
import doorthree.testtask.App;

public abstract class BaseFragment extends Fragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        enableHomeButton(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(getLayoutId(), container, false);
        initViews(view);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupViews();
    }

    @Override
    public void onResume() {
        super.onResume();
        ((AccountsActivity)getActivity()).getSupportActionBar().setTitle(getTitleId());
        App.BUS.register(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        App.BUS.unregister(this);
    }

    public void enableHomeButton(boolean enable) {
        ((AccountsActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(enable);
        ((AccountsActivity)getActivity()).getSupportActionBar().setHomeButtonEnabled(enable);
        setHasOptionsMenu(true);
    }

    public void disableHomeButton() {
        enableHomeButton(false);
    }

    public abstract int getLayoutId();
    public abstract int getTitleId();
    public abstract void initViews(View layout);
    public abstract void setupViews();
}