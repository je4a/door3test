package doorthree.testtask.fragments;

import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;

import com.google.gson.Gson;
import com.mobeta.android.dslv.DragSortListView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import doorthree.testtask.App;
import doorthree.testtask.R;
import doorthree.testtask.adapter.ContactsAdapter;
import doorthree.testtask.bus.AddContactEvent;
import doorthree.testtask.bus.SaveContactEvent;
import doorthree.testtask.bus.SaveDataEvent;
import doorthree.testtask.dto.Contact;
import doorthree.testtask.dto.ContactList;
import doorthree.testtask.util.PrefUtil;

public class ContactsFragment extends BaseFragment {
    public static final String FRAGMENT_TAG = ContactsFragment.class.getName();

    private DragSortListView lvContacts;
    private ContactsAdapter adapter;
    private List<Contact> list;

    private DragSortListView.DropListener onDrop;
    private DragSortListView.RemoveListener onRemove;
    private DragSortListView.DragScrollProfile scrollProfile;
    private int sortMode = -1; // -1 - not selected, 0 - alphabetical, 1 - non alphabetical

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        disableHomeButton();

        adapter = getAdapter(true);
        onDrop = getDropListener();
        onRemove = getRemoveListener();
        scrollProfile = getDragScrollProfile();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (adapter.isEmpty()) {
            loadContacts();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.addMenuItem) {
            App.BUS.post(new AddContactEvent(new Contact(), true));
            return true;
        } else if (id == R.id.enableDragModeMenuItem) {
            boolean dragEnabled = !lvContacts.isDragEnabled();
            changeDragMode(dragEnabled);
            return true;
        } else if (id == R.id.sortMenuItem) {
            if (sortMode == 1) { // non alphabetical
                sortMode--;
            } else { // change to alphabetical
                sortMode++;
            }

            sort();
            adapter.notifyDataSetChanged();
            saveDataToPrefs(null);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void sort() {
        switch (sortMode) {
            case 0:
                Collections.sort(adapter.getContacts(), new Comparator<Contact>() {
                    @Override
                    public int compare(Contact contact, Contact contact2) {
                        return contact.userName.compareTo(contact2.userName);
                    }
                });
                break;

            case 1:
                Collections.reverse(adapter.getContacts());
                break;
        }
    }

    public void initWithDataString(String data) {
        list = new Gson().fromJson(data, ContactList.class).contactList;
        adapter = getAdapter(false);
    }

    public String getDataString() {
        return new Gson().toJson(new ContactList(adapter.getContacts()));
    }

    private void changeDragMode(boolean dragEnabled) {
        lvContacts.setDragEnabled(dragEnabled);
        adapter.setDragEnabled(dragEnabled);
    }

    @Override
    public void onResume() {
        super.onResume();
        disableHomeButton();
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_accounts;
    }

    @Override
    public int getTitleId() {
        return R.string.contacts_title;
    }

    @Override
    public void onPause() {
        super.onPause();
        if (lvContacts.isDragEnabled()) {
            changeDragMode(false);
        }
    }

    private DragSortListView.DropListener getDropListener() {
        return new DragSortListView.DropListener() {
            @Override
            public void drop(int positionFrom, int positionTo) {
                Contact contact = adapter.getItem(positionFrom);

                adapter.remove(contact);
                adapter.insert(contact, positionTo);
                saveDataToPrefs(null);
            }
        };
    }

    @com.squareup.otto.Subscribe
    public void saveDataToPrefs(SaveDataEvent evt) {
        PrefUtil.saveDataToPreferences(getActivity(),
                new Gson().toJson(new ContactList(adapter.getContacts())));
    }

    private DragSortListView.RemoveListener getRemoveListener() {
        return new DragSortListView.RemoveListener() {
            @Override
            public void remove(int which) {
                adapter.remove(adapter.getItem(which));
                saveDataToPrefs(null);
            }
        };
    }

    private DragSortListView.DragScrollProfile getDragScrollProfile() {
        return new DragSortListView.DragScrollProfile() {
            @Override
            public float getSpeed(float w, long t) {
                //dslv lib example code
                if (w > 0.8f) {
                    // Traverse all views in a millisecond
                    return ((float) adapter.getCount()) / 0.001f;
                } else {
                    return 10.0f * w;
                }
            }
        };
    }

    @Override
    public void initViews(View view) {
        lvContacts = (DragSortListView) view.findViewById(R.id.contactsList);
    }

    @Override
    public void setupViews() {
        lvContacts.setDropListener(onDrop);
        lvContacts.setRemoveListener(onRemove);
        lvContacts.setDragScrollProfile(scrollProfile);
        lvContacts.setOnItemClickListener(getOnItemClickListener());
        lvContacts.setAdapter(adapter);
    }

    private AdapterView.OnItemClickListener getOnItemClickListener() {
        return new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                App.BUS.post(new AddContactEvent(adapter.getItem(position), false));
            }
        };
    }

    private void loadContacts() {
        String data = PrefUtil.getDataFromPreferences(getActivity());

        if (data != null && !data.isEmpty()) {
            list = new Gson().fromJson(data, ContactList.class).contactList;
        } else {
            list = new ArrayList<Contact>();
            String[] people = getResources().getStringArray(R.array.people);
            for (String human : people) {
                String split[] = human.split(",");
                Contact c = new Contact();

                c.userName = split[1].concat(" ".concat(split[0]));
                c.userEmail = split[2];
                c.location = split[3];
                list.add(c);
            }
        }

        lvContacts.setAdapter(adapter = getAdapter(list.isEmpty()));
    }

    private ContactsAdapter getAdapter(boolean empty) {
        return new ContactsAdapter(getActivity(), empty ? new ArrayList<Contact>() : list,
                lvContacts == null ? false : lvContacts.isDragEnabled());
    }

    @com.squareup.otto.Subscribe
    public void onSaveContactEvent(SaveContactEvent evt) {
        if (evt.addMode) {
            adapter.add(evt.contact);
        } else {
            if (evt.contact.id >= 0) {
                adapter.remove(adapter.getItem(evt.contact.id));
                adapter.insert(evt.contact, evt.contact.id);
            }
        }

        adapter.notifyDataSetChanged();
    }
}