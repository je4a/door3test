package doorthree.testtask.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.location.Address;
import android.location.Geocoder;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Locale;

import doorthree.testtask.App;
import doorthree.testtask.R;
import doorthree.testtask.bus.MakeToastEvent;
import doorthree.testtask.bus.PopBackStackEvent;
import doorthree.testtask.bus.SaveContactEvent;
import doorthree.testtask.dto.Contact;

@SuppressLint("ValidFragment")
public class ContactFragment extends BaseFragment implements GoogleMap.OnMapClickListener {
    public static final int SELECT_PICTURE_CODE = 1001;
    public static final int MAP_ZOOM = 17;
    public static final String FRAGMENT_TAG = ContactFragment.class.getName();
    public static final String INTENT_TYPE = "image/*";

    private static final String LOG_TAG = ContactFragment.class.getSimpleName();
    private static final File EMPTY_FILE = new File("");

    private ImageView userAvatar;
    private TextView addOrChangeAvatarTextView;
    private EditText userNameField;
    private EditText userEmailField;
    private Contact contact;
    private String imageUrl;
    private boolean addMode;

    public ContactFragment(Contact contact, boolean addMode) {
        this.contact = contact;
        this.addMode = addMode;
    }

    @Override
    public void initViews(View layout) {
        userAvatar = (ImageView) layout.findViewById(R.id.userAvatar);
        addOrChangeAvatarTextView = (TextView) layout.findViewById(R.id.addOrChangeUserAvatar);
        userNameField = (EditText) layout.findViewById(R.id.userNameField);
        userEmailField = (EditText) layout.findViewById(R.id.userEmailField);
    }

    @Override
    public void setupViews() {
        SupportMapFragment fragment = getSupportMapFragment();

        if (fragment != null && fragment.getMap() != null) {
            fragment.getMap().setOnMapClickListener(this);
        }

        setupUserAvatar();
        setupAddOrChangeAvatarView();
        setupFields();
        setupLocation();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.add_contact, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.save) {
            saveContact();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == ContactFragment.SELECT_PICTURE_CODE) {
                imageUrl = getPath(data.getData());
                loadNewAvatar(imageUrl);
                setupAddOrChangeAvatarView(true);
            }
        }
    }

    @Override
    public void onDestroyView() {
        try {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(
                    Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getActivity().getWindow().getDecorView().getWindowToken(), 0);
        } catch (Exception ex) {
            // don't care
        }

        super.onDestroyView();
        SupportMapFragment f = getSupportMapFragment();
        if (f != null) {
            getFragmentManager().beginTransaction().remove(f).commit();
        }
    }

    public SupportMapFragment getSupportMapFragment() {
        return (SupportMapFragment) getFragmentManager().findFragmentById(R.id.map);
    }

    public String getPath(Uri uri) {
        String[] projection = { MediaStore.Images.Media.DATA };
        Cursor cursor = getActivity().getContentResolver().query(uri, projection, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    private void loadNewAvatar(String url) {
        Picasso.with(getActivity())
            .load(url == null ? EMPTY_FILE : new File(url))
            .resizeDimen(R.dimen.useravatar_size, R.dimen.useravatar_size)
            .placeholder(R.drawable.ic_launcher)
            .noFade()
            .centerCrop()
            .into(userAvatar);
    }

    private void setupFields() {
        if (contact == null) {
            return;
        }

        userNameField.setText(contact.userName);
        userEmailField.setText(contact.userEmail);
    }

    private void setupLocation() {
        if (contact.location == null || contact.location.isEmpty()) {
            return;
        }

        Log.e("setupLocation", contact.location);
        if (contact.location.contains("@")) { // haven't text location, only coords
            String coordsSplit[] = contact.location.split("@");
            onMapClick(new LatLng(Double.parseDouble(coordsSplit[0]),
                    Double.parseDouble(coordsSplit[1])));
        } else {
            new AsyncTask<Void, Void, Void>() {

                @Override
                protected Void doInBackground(Void... params) {
                    onMapClick(getCoordsByAddress(contact.location));
                    return null;
                }
            }.execute();
        }
    }

    private LatLng getCoordsByAddress(String location) {
        Geocoder geocoder = new Geocoder(getActivity(), Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocationName(location, 1);

            if (addresses != null && !addresses.isEmpty()) {
                return new LatLng(addresses.get(0).getLatitude(), addresses.get(0).getLongitude());
            }
        } catch (IOException e) {
            Log.e(LOG_TAG, e.getMessage(), e);
        }

        return null;
    }

    public void saveContact() {
        if (!isUserDataValid()) {
            App.BUS.post(new MakeToastEvent(
                    getResources().getString(R.string.dataNotValidError), Toast.LENGTH_SHORT));
            return;
        }

        Contact contact;
        if (addMode) { // creating new account
            contact = new Contact();
            contact.location = this.contact.location;
        } else { // editing
            contact = this.contact;
        }

        if (imageUrl != null && !imageUrl.isEmpty()) { // in case we changed image (in edit mode)
            contact.imageUrl = imageUrl;
        }

        contact.userEmail = userEmailField.getText().toString();
        contact.userName = userNameField.getText().toString();

        App.BUS.post(new PopBackStackEvent());
        App.BUS.post(new SaveContactEvent(contact, addMode));
    }

    private void setupAddOrChangeAvatarView(boolean... change) {
        addOrChangeAvatarTextView.setText(addMode && (change == null || change.length == 0) ?
                R.string.addAvatar : R.string.changeAvatar);
    }

    private void setupUserAvatar() {
        userAvatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

                intent.setType(INTENT_TYPE);
                startActivityForResult(Intent.createChooser(intent,
                        getResources().getString(R.string.selectPicture)), SELECT_PICTURE_CODE);
            }
        });

        if (!addMode) {
            loadNewAvatar(contact.imageUrl);
        }
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_account;
    }

    @Override
    public int getTitleId() {
        return addMode ? R.string.addContact : R.string.editContact;
    }

    public boolean isUserDataValid() {
        boolean emailValid = userEmailField.getText().length() > 0 && isEmailValid(userEmailField.getText().toString());
        boolean nameValid = userNameField.getText().length() > 0;

        return nameValid && emailValid;
    }

    private boolean isEmailValid(String email) {
        // not actually best idea but for this purpose works fine
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    @Override
    public void onMapClick(final LatLng latLng) {
        if (latLng == null || !isConnectedToInternet()) {
            return;
        }

        new AsyncTask<Void, Void, Void>() {

            @Override
            protected Void doInBackground(Void... params) {
                getAccountLocation(latLng,
                        new Geocoder(getActivity(), Locale.getDefault()));

                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                SupportMapFragment mapFragment = getSupportMapFragment();
                if (mapFragment == null) {
                    return;
                }

                GoogleMap map = mapFragment.getMap();
                if (map == null) {
                    return;
                }

                map.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, MAP_ZOOM));
                map.clear();
                map.addMarker(new MarkerOptions()
                        .title(contact.location.contains("@") ? "" : contact.location)
                        .position(latLng))
                        .showInfoWindow();
            }
        }.execute();
    }

    private void getAccountLocation(LatLng latLng, Geocoder geoCoder) {
        List<Address> addresses = null;

        try {
            addresses = geoCoder.getFromLocation(latLng.latitude, latLng.longitude, 1);
        } catch (IOException e) {
            Log.e(LOG_TAG, e.getMessage(), e);
        }

        if (addresses == null || addresses.isEmpty()) {
            contact.location = latLng.latitude + "@" + latLng.longitude;
        } else {
            int count = addresses.get(0).getMaxAddressLineIndex();
            StringBuilder addressBuilder = new StringBuilder();
            for (int i = 0; i <= count; i++) {
                addressBuilder.append(addresses.get(0).getAddressLine(i).concat(" "));
            }

            contact.location = addressBuilder.toString();
        }
    }

    private boolean isConnectedToInternet() {
        ConnectivityManager connectivity = (ConnectivityManager) getActivity()
                .getSystemService(Context.CONNECTIVITY_SERVICE);

        if (connectivity != null) {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();

            if (info == null) {
                return false;
            }

            for (int i = 0; i < info.length; i++) {
                if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                    return true;
                }
            }
        }

        return false;
    }
}