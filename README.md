Test project for Door3.
**APK**: Signed and ready to use APK can be found [here](https://drive.google.com/file/d/0B5X538S4a48vRGt6WkJMdHBQMUE).

Target Android version is 4.0 and newer.

**What is done**:

1. Added list of 7 random people (it's static so people are the same even if you'll clear app data). Unfortunately, list doesn't have images, but you can add it manually.
2. Ability to manually sort the list of contacts
3. Ability to sort list in alphabetically and non-alphabetically order
4. Ability to create new contact and to change data of existing contacts
5. Ability to add the contact location
6. Ability to remove contacts from list (you can do this in manually sorting mode aka "drag'n'drop mode")

- Just press third from right button in app header if you want to enable drag'n'drop mode (manually sort the list and remove people from it). Swipe to left\right in drag mode to remove contact
- Press second from right button in app header to make an automatically sorting of the contacts list (first click sorts in alphabetically order, second in non-alphabetically and so on)
- Press first from right button in app header to add new contact
- Press on contact to change it's data


**NOTE**: use apk file (link in header of readme) if you want to be able to add location for contact

**P.S.** gitignore is not configured. sorry :(

**P.S.S.** app called "TestTask" so don't try to find it with name "Door3"